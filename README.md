# Orb for publishing protobuf generated code in an opinionated way

[![CircleCI Orb Version](https://img.shields.io/badge/endpoint.svg?url=https://badges.circleci.io/orb/benschw/protoc-tools-orb)](https://circleci.com/orbs/registry/orb/benschw/protoc-tools-orb) [![GitHub License](https://img.shields.io/badge/license-MIT-lightgrey.svg)](https://raw.githubusercontent.com/benhschwartz/protoc-tools-orb/master/LICENSE) [![CircleCI Community](https://img.shields.io/badge/community-CircleCI%20Discuss-343434.svg)](https://discuss.circleci.com/c/ecosystem/orbs)


Leverages [pb-gen](https://bitbucket.org/benhschwartz/pb-gen/src/master/) to generate and publish protobuf code for CircleCI builds

### additional setup

Add an ssh key to the project leveraging this orb to allow access to the publish to the relevant target repos